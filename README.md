# Battery Info

This is a simple battery info and history app. It displays details for all batteries and charging adapters. Additionally the charging history and statistics are shown. Currently it has about the same features as [gnome-power-statistics](https://gitlab.gnome.org/GNOME/gnome-power-manager).

download [v0.6.0](https://jloos.de/downloads/com.gitlab.jonasloos.battery-info:v0.6.0.flatpak)

![](https://jloos.de/images/battery-info.png)


# Build

Use Gnome Builder to build the app.

To update translations run the script `update-translations.sh`.
Translations are only shown when using the system runtime in Builder or when exporting the .flatpak and installing it.


# Project structure

This project uses GTK and Python and depends on dbus and upower to retrieve the device details.

Python source code is located under `src`, the ui files under `src/ui` and the translations under `po`.

Meson is used as build system.

`./flatpak-pip-generator.py --requirements-file=requirements.txt` or `./flatpak-pip-generator.py some-dependency` can be used to generate the entries for the dependencies in `com.gitlab.jonasloos.battery-info.json`.

