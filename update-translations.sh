echo "creating translations build dir"
# TODO: this is probably not the way to do it
meson _build-translations

echo "Generating .pot file..."
xgettext --from-code=UTF-8 --files-from=po/POTFILES --output=po/battery-info.pot

ninja -C _build-translations battery-info-update-po

exit 0
