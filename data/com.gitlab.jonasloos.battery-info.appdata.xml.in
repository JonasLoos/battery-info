<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
  <id>com.gitlab.jonasloos.battery-info.desktop</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>MIT</project_license>
  <name>Battery Info</name>
  <summary>View details about installed batteries and power adapters</summary>
  <description>
    <p>
      This is a simple battery info and history app. It displays details for all batteries and charging adapters.
      Additionally the charging history and statistics are shown.
    </p>
  </description>
    <screenshots>
        <screenshot type="default">
            <image>https://jloos.de/images/battery-info.png</image>
        </screenshot>
    </screenshots>
  <url type="homepage">https://gitlab.com/JonasLoos/battery-info</url>
  <url type="bugtracker">https://gitlab.com/JonasLoos/battery-info/-/issues</url>
  <releases>
    <release version="0.6.0" timestamp="1608999339">
      <description>
        <ul>
          <li>improved dark mode support</li>
          <li>improved history plot</li>
          <li>select time range for history plot</li>
          <li>improved german translations</li>
        </ul>
      </description>
    </release>
    <release version="0.5.1" timestamp="1607955529">
      <description>
        <ul>
          <li>details: format numbers to two digits</li>
          <li>update to gnome 3.38 runtime</li>
        </ul>
      </description>
    </release>
    <release version="0.5.0" timestamp="1584802829">
      <description>
        <ul>
          <li>improved layout for device details</li>
          <li>remove references to not yet implemented features</li>
        </ul>
      </description>
    </release>
    <release version="0.4.0" timestamp="1584087109">
      <description>
        <ul>
          <li>refresh device info when requested or a device is added/removed</li>
          <li>better history graphs</li>
          <li>scroll on history page</li>
          <li>update translations</li>
        </ul>
      </description>
    </release>
    <release version="0.3.0" timestamp="1582037009">
      <description>
        <ul>
          <li>update keyboard shortcuts window</li>
          <li>make window scrollable</li>
          <li>update translations</li>
          <li>add icons to devices list</li>
          <li>add app icon</li>
          <li>fix history and statistics plotting</li>
        </ul>
      </description>
    </release>
    <release version="0.2.0" timestamp="1581341810">
      <description>
        <ul>
          <li>add keyboard shortcuts</li>
          <li>add keyboard shortcuts window</li>
          <li>rewrite logging routines</li>
          <li>add menu</li>
          <li>update translations</li>
        </ul>
      </description>
    </release>
    <release version="0.1.0" timestamp="1581341809">
      <description>
        <ul>
          <li>basic application and first attempts to use flatpak builds</li>
        </ul>
      </description>
    </release>
  </releases>
  <content_rating type="oars-1.1" />
</component>
