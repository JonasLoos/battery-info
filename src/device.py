# device.py
#
# Copyright 2020 Jonas Loos
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

import time
import dbus

from gi.repository import Gtk
from gettext import gettext as _

# plotting stuff
import numpy as np
import matplotlib.dates as mdates
from datetime import datetime

from .utils import log, err

class Upower_device:
	"""a dbus upower device like a battery or an adapter"""
	def __init__(self, path):
		"""init interfaces and get type"""
		obj = dbus.SystemBus().get_object('org.freedesktop.UPower', path)
		self.prop_iface = dbus.Interface(obj, dbus_interface='org.freedesktop.DBus.Properties')
		self.device_iface = dbus.Interface(obj, dbus_interface='org.freedesktop.UPower.Device')

		# set the type of the device
		device_type = self.get('Type')
		self.type = ['Unknown','Line Power','Battery','Ups','Monitor','Mouse','Keyboard','Pda','Phone'][device_type]
		self.type_text = [_('Unknown'),_('Line Power'),_('Battery'),_('Ups'),_('Monitor'),_('Mouse'),_('Keyboard'),_('Pda'),_('Phone')][device_type]

	def __str__(self):
		text = f'{self.type_text} {self.get("NativePath")}'
		if self.type == 'Battery':
			text += f': {self.get("Percentage")}%'
		if self.type == 'Line Power' and self.get('Online'):
			text += ': online'
		return text

	def get(self, prop):
		"""get a property of the device
			available properties:
				NativePath,
				Vendor,
				Model,
				Serial,
				UpdateTime,
				Type,
				PowerSupply,
				HasHistory,
				HasStatistics,
				Online,
				Energy,
				EnergyEmpty,
				EnergyFull,
				EnergyFullDesign,
				EnergyRate,
				Voltage,
				Luminosity,
				TimeToEmpty,
				TimeToFull,
				Percentage,
				Temperature,
				IsPresent,
				State,
				IsRechargeable,
				Capacity,
				Technology,
				WarningLevel,
				BatteryLevel,
				IconName
		"""
		try:
			return self.prop_iface.Get('org.freedesktop.UPower.Device', prop)
		except Exception as e:
			err(f"error getting property '{prop}': {e.args}")

	def set_details_page(self, title, grid):
		"""build the details page depending on the type of this device and the set properties. Unknown properties are hidden."""

		def time_format(seconds):
			'''convert time from seconds to a human readable format'''
			if not seconds: return
			result = ''
			hours = int(seconds/3600)
			minutes = int((seconds%3600)/60)
			if hours > 0:
				# hours
				result += str(hours) + _('h')
				if minutes > 0:
					# minutes
					result += ' ' + str(minutes) + _('min')
			elif minutes > 0:
				# minutes
				result += str(minutes) + _('min')
			else:
				# seconds
				result += str(seconds) + _('s')
			return result

		def num_format(value):
			return '{:.2f}'.format(value) if value else ''

		log(f"show details of device '{self.get('NativePath')}'")

		# stuff for all types
		title.set_markup(f'<b><big>{self.type_text} {self.get("NativePath")}</big></b>')
		title.set_halign(1)
		for item in grid.get_children():
			item.destroy()

		lines = []

		# battery
		if self.type == 'Battery':
			# human readable battery strings from https://upower.freedesktop.org/docs/Device.html

			# progress bar
			progress = Gtk.ProgressBar()
			perc = self.get('Percentage')/100
			progress.set_fraction(perc)
			progress.set_show_text(f'{perc}%')
			grid.attach(progress, 0, 0, 2, 1)
			lines += [(_('Battery Properties'),)]
			# The battery power state.
			state = self.get('State')
			if state > 0: lines += [(_('State'), [_('Unknown'),_('Charging'),_('Discharging'),_('Empty'),_('Fully charged'),_('Pending charge'),_('Pending discharge')][state])]
			# Level of the battery.
			bat_level = self.get('BatteryLevel')
			if bat_level > 1: lines += [(_('Battery Level'), [_('Unknown'),_('None'),_('Discharging'),_('Low'),_('Critical'),_('Action')][bat_level])]
			# Warning level of the battery.
			warning_level = self.get('WarningLevel')
			if warning_level > 1: lines += [(_('Warning Level'), [_('Unknown'),_('None'),_('Discharging'),_('Low'),_('Critical'),_('Action')][warning_level])]
			# Technology used in the battery.
			tech = self.get('Technology')
			if tech > 0: lines += [(_('Technology'), [_('Unknown'),_('Lithium ion'),_('Lithium polymer'),_('Lithium iron phosphate'),_('Lead acid'),_('Nickel cadmium'),_('Nickel metal hydride')][tech])]
			# Amount of energy (measured in Wh) currently available in the power source.
			lines += [(_('Energy'), num_format(self.get('Energy')), 'Wh')]
			# Amount of energy (measured in Wh) in the power source when it's considered to be empty.
			lines += [(_('Energy (Empty)'), num_format(self.get('EnergyEmpty')), 'Wh')]
			# Amount of energy (measured in Wh) in the power source when it's considered full.
			lines += [(_('Energy (Full)'), num_format(self.get('EnergyFull')), 'Wh')]
			# Amount of energy (measured in Wh) the power source is designed to hold when it's considered full.
			lines += [(_('Energy (Full, Design)'), num_format(self.get('EnergyFullDesign')), 'Wh')]
			# Amount of energy being drained from the source, measured in W. If positive, the source is being discharged, if negative it's being charged.
			lines += [(_('Energy-Rate'), num_format(self.get('EnergyRate')), 'W')]
			# Number of seconds until the power source is considered empty. Is set to 0 if unknown.
			lines += [(_('Time to empty'), time_format(self.get('TimeToEmpty')))]
			# Number of seconds until the power source is considered full. Is set to 0 if unknown.
			lines += [(_('Time to full'), time_format(self.get('TimeToFull')))]
			# The temperature of the device in degrees Celsius.
			lines += [(_('Temperature'), self.get('Temperature'), '°C')]
			# If the power source is present in the bay. This field is required as some batteries are hot-removable, for example expensive UPS and most laptop batteries.
			lines += [(_('Present'), _('yes') if self.get('IsPresent') else _('no'))]
			# If the power source is rechargeable.
			lines += [(_('Rechargeable'), _('yes') if self.get('IsRechargeable') else _('no'))]
			# The capacity of the power source expressed as a percentage between 0 and 100. The capacity of the battery will reduce with age. A capacity value less than 75% is usually a sign that you should renew your battery. Typically this value is the same as (full-design / full) * 100. However, some primitive power sources are not capable reporting capacity and in this case the capacity property will be unset.
			lines += [(_('Capacity'), num_format(self.get('Capacity')), '%')]

		# line-power
		elif self.type == 'Line Power':
			lines += [(_('Line Power Properties'),)]
			# Whether power is currently being provided through line power. This property is only valid if the property type has the value "line-power".
			lines += [(_('Online'), _('yes') if self.get('Online') else _('no'))]

		lines += [(_('Device Properties'),)]
		# Name of the vendor of the battery.
		lines += [(_('Vendor'), self.get('Vendor'))]
		# Name of the model of this battery.
		lines += [(_('Model'), self.get('Model'))]
		# Unique serial number of the battery.
		lines += [(_('Serial'), self.get('Serial'))]
		# If the power device is used to supply the system. This would be set TRUE for laptop batteries and UPS devices, but set FALSE for wireless mice or PDAs.
		lines += [(_('PowerSupply'), _('yes') if self.get('PowerSupply') else '')]
		# Voltage in the Cell or being recorded by the meter.
		lines += [(_('Voltage'), self.get('Voltage'), 'V')]
		# Luminosity being recorded by the meter.
		lines += [(_('Luminosity'), self.get('Luminosity'))]

		def new_label(text, halign=0):
			"""creates a new Gtk.Label with line wrap enabled"""
			label = Gtk.Label()
			label.set_markup(text)
			label.set_line_wrap(True)
			label.set_halign(halign)
			return label

		# insert all the data into the grid
		for i, (text, *values) in enumerate(lines, 1):
			# property line
			if values:
				value, unit, *unused = values + ['']
				if value:
					grid.attach(new_label(f'<span foreground="grey">{text}</span>', halign=2), 0, i, 1, 1)
					grid.attach(new_label(f'{value}{unit}', halign=1), 1, i, 1, 1)
			else:
				# heading line
				heading = new_label(f'<b>{text}</b>')
				heading.set_margin_top(20)
				heading.set_hexpand(True)  # this makes the whole grid expand
				grid.attach(heading, 0, i, 2, 1)

		# display the properties
		grid.show_all()


	def plot_history(self, subplot, hist_type, hist_time, color):
		"""get history or statistics data for the graph"""

		# parse timerange
		time_ranges = {0: 7*24, 1: 24, 2: 10, 3: 4}  # id to hours backwards in time
		timespan = time_ranges[hist_time]*60*60 if hist_time in time_ranges else err(f'error: unknown time range `{hist_time}`') or 0

		# check if the requested history type exists
		if hist_type not in [0,1,2,3]:
			err(f'error: unknown history type `{history_type}`')
			return
		if (hist_type in [0,1] and not self.get('HasHistory')) or (hist_type in [2,3] and not self.get('HasStatistics')):
			# no data exists for this device
			return

		# get the requested history type and set the labels
		if hist_type in [0,1]:  # history
			subplot.set_xlabel(_('Time'))
			subplot.set_ylabel(_('Charge [%]') if hist_type == 0 else _('Rate [W]'))
			subplot.xaxis.set_major_formatter(mdates.DateFormatter('%a' if hist_time == 0 else '%H:%M'))
			subplot.xaxis.set_major_locator(mdates.AutoDateLocator(minticks=3, maxticks=7))
			subplot.set_xlim(datetime.fromtimestamp(datetime.now().timestamp() - timespan), datetime.now())
			if hist_type == 0:
				subplot.set_ylim(0,100)

			# GetHistory(type, timespan, resolution)
			data = np.array(self.device_iface.GetHistory('charge' if hist_type == 0 else 'rate', timespan, 1000000))  # args: (type, timespan, resolution)
			# data[:,0]: time: The time value in seconds from the gettimeofday() method.
			# data[:,1]: value: The data value, for instance the rate in W or the charge in %.
			# data[:,2]: state: The state of the device, for instance charging or discharging.
				# 0: Unknown
				# 1: Charging
				# 2: Discharging
				# 3: Empty
				# 4: Fully charged
				# 5: Pending charge
				# 6: Pending discharge

			if len(data) == 0:
				# no data
				log(f'upower GetHistory returned no data for {self.get("NativePath")} (time: {timespan})')
				return

			values = data[:,1]
			times = list(map(datetime.fromtimestamp, data[:,0]))
			if hist_type == 1:  # history rate
				# display negative values for discharging
				values = np.array([(-x[1] if x[2] == 2 else x[1]) for x in data])

			# insert gaps when there is nothing for a long time
			times_with_gaps = [times[1]]
			values_with_gaps = [values[1]]
			for i in range(1,len(data)):
				if data[i-1,0] - data[i,0] > 15*60:  # more than 15min; TODO: think about if 15min makes sense
					times_with_gaps.append(None)
					values_with_gaps.append(np.nan)
				times_with_gaps.append(times[i])
				values_with_gaps.append(values[i])

			# plot data
			subplot.plot(times_with_gaps, values_with_gaps, color=color, label=self.get('NativePath'))
			subplot.scatter(times, values, s=1, color=color)  # dots for data points

		else:  # statistics
			subplot.set_xlabel(_('Charge [%]'))
			subplot.set_ylabel(_('Correction Factor [s]'))

			statistics = np.array(self.device_iface.GetStatistics('charging' if hist_type == 2 else 'discharging'))
			# statistics[:,0]: value: The value of the percentage point, usually in seconds
			# statistics[:,1]: accuracy: The accuracy of the prediction in percent.

			if len(statistics) == 0:
				# no data
				log(f'upower GetStatistics returned no data for {self.get("NativePath")} (time: {timespan})')
				return

			# add labels with correct color
			subplot.plot([],[],color=color, label=self.get('NativePath'))
			# plot data
			subplot.scatter(range(101), statistics[:,0], c=(np.array(statistics[:,1])/100), alpha=0.5, cmap=color+'s', edgecolor='none', s=20)

