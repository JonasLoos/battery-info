# window.py
#
# Copyright 2020 Jonas Loos
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

from gi.repository import Gtk, Gio

#from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas  # old -> has a low resulution
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
from matplotlib.figure import Figure
import matplotlib as mpl

from .utils import log, err
from .device import Upower_device

import time
import dbus
import dbus.mainloop.glib


UI_PATH = '/com/gitlab/jonasloos/battery-info/ui/'


@Gtk.Template(resource_path=UI_PATH+'window.ui')
class BatteryInfoWindow(Gtk.ApplicationWindow):
	"""the main application window"""
	__gtype_name__ = 'BatteryInfoWindow'

	# main_window = Gtk.Template.Child()
	about_window = Gtk.Template.Child()
	shortcuts_window = Gtk.Template.Child()
	main_stack = Gtk.Template.Child()
	devices_stack = Gtk.Template.Child()
	history_page = Gtk.Template.Child()
	history_canvas_box = Gtk.Template.Child()
	devices_list_box = Gtk.Template.Child()
	devices_list = Gtk.Template.Child()
	devices_details_box = Gtk.Template.Child()
	devices_details_title = Gtk.Template.Child()
	devices_details_grid = Gtk.Template.Child()
	history_chooser = Gtk.Template.Child()
	history_time = Gtk.Template.Child()
	menu_btn = Gtk.Template.Child()
	history_page_graph_type_label = Gtk.Template.Child()

	# save which device details page is displayed to be able to refresh that page
	current_device_details_page = 0

	# save last refresh time to avoid to multiple refreshs which could impact responsiveness
	last_refresh = 0

	# dbus devices like power adapters and batteries
	dbus_devices = []

	def __init__(self, **kwargs):
		super().__init__(**kwargs)

		log("init Ui")

		# set link to app class and back
		self.app = kwargs['application']
		self.app.ui = self

		# set default window size (width, height)
		self.set_default_size(800, 600)

		# menu
		menu_builder = Gtk.Builder().new_from_resource(UI_PATH + 'menus.ui')
		self.menu_btn.set_menu_model(menu_builder.get_object('app-menu'))

		# setup Keyboard shortcuts / actions
		self.init_actions()
		self.init_plotting_area()
		self.init_dbus()
		self.refresh()
		self.plot()

		# react to theme changes
		Gtk.Settings.get_default().connect("notify::gtk-theme-name", self.on_theme_name_changed)

	def init_devices_list(self):
		"""set up the list of devices"""

		# remove existing devices
		for row in self.devices_list.get_children():
			self.devices_list.remove(row)

		# add a row for each device
		for device in self.dbus_devices:
			# box
			box = Gtk.Box()
			box.set_margin_top(5)
			box.set_margin_bottom(5)
			box.set_margin_start(10)
			box.set_margin_end(10)
			# icon
			icon = Gtk.Image.new_from_icon_name(device.get('IconName'), 0)
			icon.set_margin_end(10)
			box.pack_start(icon, False, True, 0)
			# text
			text = Gtk.Label(str(device))
			text.set_xalign(0)
			box.pack_start(text, True, True, 0)

			self.devices_list.add(box)

		# show added items
		self.devices_list.show_all()

	def init_plotting_area(self):
		"""add a FigureCanvas to the history tab"""

		# clear old canvas
		if hasattr(self, 'canvas') and self.canvas:
			self.subplot.clear()
			self.history_canvas_box.remove(self.canvas)

		# set up the history graph figure
		fig = Figure(dpi=100, facecolor=(0,0,0,0))  # figsize=(10, 8) was added originally, but has no effect
		self.subplot = fig.add_subplot(111, facecolor=(0,0,0,0))

		# disable borders
		for border in ['top','right','bottom','left']:
			self.subplot.spines[border].set_visible(False)

		# set text color
		tmp = self.history_page_graph_type_label.get_style_context().get_color(Gtk.StateFlags.NORMAL)
		textcolor = (tmp.red, tmp.green, tmp.blue, tmp.alpha)
		self.subplot.spines['bottom'].set_color(textcolor)
		self.subplot.spines['left'].set_color(textcolor)
		self.subplot.xaxis.label.set_color(textcolor)
		self.subplot.yaxis.label.set_color(textcolor)
		self.subplot.tick_params(axis='x', colors=textcolor)
		self.subplot.tick_params(axis='y', colors=textcolor)
		mpl.rcParams['text.color'] = textcolor  # set textcolor of the legend
		mpl.rcParams['legend.framealpha'] = 0  # transparent legend background

		# setup canvas
		self.canvas = FigureCanvas(fig)
		self.canvas.set_size_request(0, 500)  # x-size (irrelevant), y-size
		self.history_canvas_box.add(self.canvas)
		self.history_canvas_box.show_all()

	def init_actions(self):
		"""initializes the keyboard shortcuts and actions"""

		# list of functions called when the declared keys are pressed or the action is called otherwise (e.g. menu)
		actions = [
			(self.show_about, None),
			(self.go_back, ['BackSpace', '<Alt>Left', 'Escape']),
			(self.show_shortcuts, ['<Ctrl>question']),
			(self.refresh, ['F5', '<Ctrl>r']),
			# Alt+i for pages
			*((self.show_page, [f'<Alt>{str(i+1)}'], i) for i in range(2)),
			# 1-9 for some action based on shown page (like opening the details for device i)
			*((self.num_pressed, [str(i+1)], i) for i in range(9)),
		]

		# set the actions and shortcuts and use the callback function name as the action name
		for func, shortcuts, *args in actions:
			name = f'{func.__name__}-{args[0]}' if args else func.__name__  # FIXME: args[0] might not be unique for more than 1 arg
			action = Gio.SimpleAction.new(name, None)
			action.connect('activate', func, *args)
			self.app.add_action(action)
			if shortcuts:
				self.app.set_accels_for_action('app.'+name, shortcuts)

	def init_dbus(self):
		"""get battery and adapter info from dbus"""

		# connect dbus to the main loop to be able to receive signals
		dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
		# get the upower dbus object
		self.upower = dbus.SystemBus().get_object('org.freedesktop.UPower', '/org/freedesktop/UPower')
		# connect to the device added/removed signals to refresh the device list when needed
		self.upower.connect_to_signal('DeviceAdded', self.refresh)
		self.upower.connect_to_signal('DeviceRemoved', self.refresh)

	def refresh(self, *args):
		"""get new data about devices"""
		now = time.time()
		if now - self.last_refresh < 0.25:  # TODO: think about whether or not this is necessary
			# last refresh was less than 0.25 seconds ago
			# so don't refresh again to improve responsiveness
			log('refresh not needed')
			return
		log('refresh')
		self.last_refresh = now

		# TODO: maybe call dbus refresh method on each device

		# if the details page is displayed, only refresh the shown device
		if self.main_stack.get_visible_child() == self.devices_stack and self.devices_stack.get_visible_child() == self.devices_details_box:
			self.show_device_details(self.current_device_details_page)
			return

		# get and initialize all upower/dbus devices
		# TODO: maybe add system/display device with additional infos like screen brightness, ...
		devices = self.upower.EnumerateDevices(dbus_interface='org.freedesktop.UPower')
		self.dbus_devices = [Upower_device(device_path) for device_path in devices]

		# refresh ui
		self.init_devices_list()
		self.plot()

	def show_page(self, bla, foo, page):
		"""shows the page (Devices/History/Usage) given as argument"""
		log(f'show page {page}')
		self.main_stack.set_visible_child(self.main_stack.get_children()[page])

	def num_pressed(self, bla, foo, i):  # FIXME
		"""Handles a press of a single number 1-9. Calls some function based on which page is open."""
		visible = self.main_stack.get_visible_child()
		if visible == self.devices_stack:
			self.show_device_details(i)
		elif visible == self.history_page:
			# choose which graph to plot
			if i < 4:
				self.history_chooser.set_active(i)

	def show_device_details(self, device_num):
		"""display a detailed view of the properties of the given device"""
		# make sure the device exists
		if 0 <= device_num < len(self.dbus_devices):
			self.current_device_details_page = device_num
			# build and show details page
			self.dbus_devices[device_num].set_details_page(self.devices_details_title, self.devices_details_grid)
			self.devices_stack.set_visible_child(self.devices_details_box)

	def plot(self):
		"""plot the history/statistics for all devices which support it"""
		history_type = self.history_chooser.get_active()
		history_time = self.history_time.get_active()

		# disable time chooser if statistics are selected
		self.history_time.set_sensitive(history_type in [0,1])


		self.subplot.clear()
		colors = ['Blue', 'Orange', 'Purple', 'Green', 'Red']*(1+len(self.dbus_devices)//5)
		for device, color in zip(self.dbus_devices, colors):
			device.plot_history(self.subplot, history_type, history_time, color)
		# update the canvas
		self.subplot.legend()
		self.canvas.draw()

	def on_theme_name_changed(self, settings, gparam):
		print("Detected theme change to:", settings.get_property("gtk-theme-name"))
		self.init_plotting_area()
		self.plot()

	@Gtk.Template.Callback('go_back')
	def go_back(self, *args):
		"""leave the details page and go back to the device list"""
		if self.main_stack.get_visible_child() == self.devices_stack:
			self.devices_stack.set_visible_child(self.devices_list_box)

	@Gtk.Template.Callback('show_device_details_handler')
	def show_device_details_handler(self, *args):
		self.show_device_details(args[1].get_index())

	@Gtk.Template.Callback('graph_type_changed')
	def graph_type_changed(self, widget):
		log(f'switch to graph type {widget.get_active()}')
		self.plot()

	@Gtk.Template.Callback('graph_time_changed')
	def graph_time_changed(self, widget):
		log(f'switch to graph time range {widget.get_active()}')
		self.plot()


### WINDOW HANDLERS

	def show_about(self, *args):
		"""display the about window"""
		log("show about window")
		self.about_window.show_all()
		return True

	@Gtk.Template.Callback('hide_about')
	def hide_about(self, *args):
		"""close the about window"""
		self.about_window.hide()
		return True

	def show_shortcuts(self, *args):
		"""display the keyboard shortcuts overview window""" 
		log("show shortcuts")
		self.shortcuts_window.show_all()
		return True

	@Gtk.Template.Callback('hide_shortcuts')
	def hide_shortcuts(self, *args):
		"""close the keyboard shortcuts overview window"""
		self.shortcuts_window.hide()
		return True
