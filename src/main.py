# main.py
#
# Copyright 2020 Jonas Loos
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

import sys
import gi

# gtk stuff
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, GLib

# from this app
from .window import BatteryInfoWindow
from .utils import log, err


class Application(Gtk.Application):
	def __init__(self, version='0.0.0'):
		"""initialize the app"""
		super().__init__(application_id='com.gitlab.jonasloos.battery-info',
						 flags=Gio.ApplicationFlags.FLAGS_NONE)

		log("init App")

		self.version = version

		# TODO: is this necessary?
		GLib.set_application_name("BatteryInfo")
		GLib.set_prgname('com.gitlab.jonasloos.battery-info')

	def do_activate(self):
		"""called when the app is starting"""
		ui = self.props.active_window
		if not ui:
			ui = BatteryInfoWindow(application=self)
			self.ui.about_window.set_version(self.version)
			ui.show()
		ui.present()


def main(version):
	"""run the app"""
	app = Application(version=version)
	return app.run(sys.argv)
