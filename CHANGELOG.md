pre 0.6.1
=====
* high resolution plots
* gaps in plots where data is missing
* update plot color on theme change

0.6.0
=====
* improved dark mode support
* improved history plot
* select time range for history plot
* improved german translations

0.5.1
=====
* details: format numbers to two digits
* update to gnome 3.38 runtime

0.5.0
=====
* improved layout for device details
* remove references to not yet implemented features

0.4.0
=====
* refresh device info when requested or a device is added/removed
* better history graphs
* scroll on history page
* update translations

0.3.0
=====
* update keyboard shortcuts window
* make window scrollable
* update translations
* add icons to devices list
* add app icon
* fix history and statistics plotting

0.2.0
=====
* add keyboard shortcuts
* add keyboard shortcuts window
* rewrite logging routines
* add menu
* update translations

0.1.0
=====
* basic application and first attempts to use flatpak builds

